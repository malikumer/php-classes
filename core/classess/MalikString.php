<?php
/**
 * Malik Corporation private limited
 *
 * @package   (my-mos.com)
 * @author    Malik Umer Farooq<admin@my-mos.com>
 * @copyright 2016-2017 Malik Corporation private limited
 * @license  Malik Corporation private limited https://my-mos.com/public/terms
 * @link     https://my-mos.com/public/
 */
class Strings
{
        /**
         * Remove abusive/anytype of word in the string or user input
         * @param  $params (array)
         * 'replacing_word' =>  rword need to search either come form database or you 
         * written in array
         * 'replacing_with' => Word need to replace with these words
         * 'text' => User input or anyString
         * ISSUE: text uppercase and lowercase issues
         * @return string
        */   	
	public function MalikReplaceWords($params){
		if(is_array($params)){
			if(!empty($params['replacing_word']) and !empty($params['replacing_with'])){
				$replacing_word = $params['replacing_word'];
				$replacing_with = $params['replacing_with'];
				return str_replace($replacing_word, $replacing_with, $params['text']); 
			}
			else{
				return faslse;
			}
		}else{
			return false;
		}
	}
        /**
         * Convert uppercase to lower & lowercase to upper
         * @param $params (array)
         * 'type' => 
         * 		'secured' =  remove whitespace html tag convert to entities and secured
         * 		'root' = extra feature then secured Convert all applicable characters to HTML entities
         * 'input' => User input or anyString
         * @return string
        */   	
	public function MalikCleanInput($params){
		if(is_array($params)){
			if(!empty($params['input'])){
				if(!empty($params['type'])){
					if($params['type'] === 'secured'){
				        return  stripslashes(trim(htmlspecialchars($params['input'])));
					}elseif($params['type'] === 'root'){
						return  stripslashes(trim(htmlspecialchars(strip_tags($params['input']))));
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false; 
		}
	}

        /**
         * Convert uppercase to lower & lowercase to upper
         * @param   $params (array)
         * 'type' => possible uppercase and lowercase
         * 'text' => string to conversion
         * @return string
        */    
	public function MalikStringConversion ($params){
		if(is_array($params)){
			if(!empty($params['text'])){
				if($params['type'] === 'lowercase'){
					if(function_exists('strtolower')){
						return strtolower($params['text']);
					}else{
						  $MalikConvertTo = [
						    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
						    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
						    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
						    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
						    "ь", "э", "ю", "я" 
						  ]; 
						  $MAlikConvertFrom = [
						    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
						    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
						    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
						    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
						    "Ь", "Э", "Ю", "Я" 
						  ]; 	
							 return str_replace($MAlikConvertFrom, $MalikConvertTo, $params['text']); 
					}
				}elseif($params['type'] === 'uppercase'){
					if(function_exists('strtoupper')){
						return strtoupper($params['text']);
					}else{
						  $MalikConvertTo = [
						    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
						    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
						    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
						    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
						    "Ь", "Э", "Ю", "Я" 
						  ]; 
						  $MAlikConvertFrom = [
						    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
						    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
						    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
						    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
						    "ь", "э", "ю", "я" 
						  ]; 					  	
							 return str_replace($MAlikConvertFrom, $MalikConvertTo, $params['text']); 
					}
				}else{
					return false;
				}
		}else{
			return false;
		}
	}else{
			return false;
		}
	}
}

