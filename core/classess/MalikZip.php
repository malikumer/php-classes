<?php
/**
 * Malik Corporation private limited
 *
 * @package   (my-mos.com)
 * @author    Malik Umer Farooq<admin@my-mos.com>
 * @copyright 2016-2017 Malik Corporation private limited
 * @license  Malik Corporation private limited https://my-mos.com/public/terms
 * @link     https://my-mos.com/public/
 */
class Zip
 {
	 	/**
		 * Open zip extract zip 
		 * @file file to extract
		 * @file_extract_to path
		 * @return boolean
		 */	 
	public function MalikOpenZip($file,$file_extract_to){
		$zip = new ZipArchive();
		$x = $zip->open($file);
		if($x === true){
			$zip->extractTo($file_extract_to);
			$zip->close();
			unlink($file);	
		}
	}
		 /**
		 * Compressed zip 
		 * @files files to compressed
		 * @destination path
		 * @overwrite overright if already
		 * @return boolean
		 */	
	public function MalikCreateZip($files = array(),$destination = '',$overwrite = false) {
	    //if the zip file already exists and overwrite is false, return false
	    if(file_exists($destination) && !$overwrite) { return false; }
	    //vars
	    $valid_files = array();
	    //if files were passed in...
	    if(is_array($files)) {
	        //cycle through each file
	        foreach($files as $file) {
	            //make sure the file exists
	            if(file_exists($file)) {
	                $valid_files[] = $file;
	            }
	        }
	    }
	    //if we have good files...
	    if(count($valid_files)) {
	        //create the archive
	        $zip = new ZipArchive();
	        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
	            return false;
	        }
	        //add the files
	        foreach($valid_files as $file) {
	            $zip->addFile($file,$file);
	        }
	        //close the zip -- done!
	        $zip->close();
	        
	        //check to make sure the file exists
	        return file_exists($destination);
	    }
	    else
	    {
	        return false;
	    }
	}	 
 }