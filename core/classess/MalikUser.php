<?php
/**
 * Malik Corporation private limited
 *
 * @package   (my-mos.com)
 * @author    Malik Umer Farooq<admin@my-mos.com>
 * @copyright 2016-2017 Malik Corporation private limited
 * @license  Malik Corporation private limited https://my-mos.com/public/terms
 * @link     https://my-mos.com/public/
 */
class User
{
		/**
		 * Initialize the objects.
		 *
		 * @return void
		 */	
	public function InitObjects(){
		$this->Database = new Database;
		$this->Site = new Site;
	}
		/**
		 * Get all the users
		 *
		 * @return array
		 */		
	public function MalikGetAUser($params){
		self::InitObjects();
		if(is_array($params)){
			return $this->Database->MalikSelectRecord($params);
		}else{
			return false;
		}
		
	}
		/**
		 * Get user by values
		 * @param => (array)
		 * 'table' => (string) Name of table
		 * 'field' => (string) value 
		 *
		 * @return object
		 */			
	public function MalikGetUser($params){
		self::InitObjects();
		if(is_array($params)){
			$data = malik_array_object($params);
			if(isset($data->{key($data)})){
				$key = key($data);
				$value = $this->Database->MalikQuote($data->{key($data)});
			}
			return $this->Database->MalikSelectRecord(['from'=>'users','wheres'=>["{$key} = ".$value ]]);
		}else{
			return false;
		}
	}
		/**
		 * Adding users to database
		 * @param =>  $params (array)
		 * 'table' => (string) Name of table
		 * 'password' => (string) Password 
		 * 'data' (array) additional fields like name=>value
		 *
		 * @return lastid
		 */		
	public function MalikSignup($params){
		self::InitObjects();
		if(is_array($params)){
			if(!empty($params['table'])){
				$old_password = $params['password'];
				$hash = $this->Site->MalikGenerateSalts(8);
				$password = crypt($old_password,$hash);
				unset($old_password);
				unset($hash);
				$token = $this->Site->MalikGenerateSalts(8);
				$salts = $this->Site->MalikGenerateSalts(6);
				$rtoken = $this->Site->MalikGenerateSalts(10);
				$data1 = $params['data'];
				$data2 = [
					'password' => $password,
					'token' => $token,
					'salts' => $salts,
					'rtoken' => $rtoken,
					'created' => time(),
				];
				$data_final = array_merge($data1,$data2);
				unset($password);
				unset($token);
				unset($salts);
				unset($rtoken);
				$insert = $this->Database->MalikInsert($params['table'], $data_final);
				return $insert;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
		/**
		 * Check users specific value exists or not
		 *
		 * @param => $params (array)
		 * 'table' => Name of database table
		 * 'key' => Key to find e.g username
		 * 'value' => value of key e.g jones
		 * @return boolean 
		 */		
	public function MalikCheckRecord($params){
		self::InitObjects();
		if(is_array($params)){
			if($this->Database->MalikCountRecord([
				'table'=>$params['table'],
				'wheres'=>[$params['key'].' = '. $this->Database->MalikQuote($params['value']
			)]])){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
		/**
		 * Login in the users adding session and cookies
		 * @param =>$params (array)
		 * 'table' => (string) Name of table
		 * 'username' => (string) Username
		 * 'password' => (string) Password 
		 *
		 * @return boolean on success 
		 * @errors codes
		 * '20001' Password not matched
		 * '20002' username not found
		 */		
	public function MalikLogin($params){
		self::InitObjects();
		if(is_array($params)){
			if(!empty($params['username']) and !empty($params['password'])){
				$username = $this->Database->MalikQuote($params['username']);
				$password = $params['password'];
				$row = $this->Database->MalikCountRecord([
					'table' => $params['table'],
					'wheres' => ['username = '.$username]]);
				if($row !== 0 or $row === 1){
					$select = $this->Database->MalikSelectRecord(['from'=>$params['table'],
						'wheres'=>[ 'username = '.$username]]);
					if(password_verify($password,$select->{0}->password)){
						unset($password);
						unset($select->{0}->password);
						if($select->{0}->token === null){
						unset($select->{0}->token);	
						if (session_status() === PHP_SESSION_NONE) {
    						session_start();
						}
						$_SESSION['malik_users'] = $select->{0}->salts;
						setcookie('malik_cookie_users',$select->{0}->salts,time() + (86400 * 30 * 12), '/',$_SERVER['SERVER_NAME'],true,true);
						 return true;
					}else{
						return 20003;
					}
					}else{
						return 20001;
					}
				
				}else{
					return 20002;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
		/**
		 * Logout to users
		 *
		 * @return boolean 
		 */	
	public function MalikLogout(){
		if (session_status() === PHP_SESSION_NONE) {
    		session_start();
		}		
		if(isset($_SESSION['malik_users'])){
			session_destroy();
			session_unset();
			if(isset($_COOKIE['malik_cookie_users'])){
				setcookie('malik_cookie_users',$_COOKIE['malik_cookie_users'],time() - 3600, '/',$_SERVER['SERVER_NAME'],true,true);
			}	
		}
		return true;
	}
} 