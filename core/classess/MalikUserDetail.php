<?php
class UserDetail
{   
        /**
         * Initialize the objects.
         *
         * @return void
         */         
    public function InitObjects(){
        $this->Json = new Json;
    }
        /**
         * Get ip address of user
         *
         * @return int
         */       
    public function MalikIp() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])){
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        }
        else if(isset($_SERVER['REMOTE_ADDR'])){
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        }
        else{
            $ipaddress = 'UNKNOWN';
        }
        return $ipaddress;
    }
        /**
         * Get country name using api
         *
         * @return string
         */           
    public function MalikCountry(){
        self::InitObjects();
        //$ip = $this->MalikIp();
        $ip = '119.152.36.222';
        $data =  $this->Json->MalikDecode(file_get_contents("http://ipinfo.io/{$ip}"));
        return $data->country;
    }
        /**
         * Get user details
         *
         * @return string
         */          
    public function MalikUserDetail(){ 
        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) { 
            $browser_name = 'Internet Explorer'; 
            $user_a = "MSIE"; 
        } elseif(preg_match('/Firefox/i',$u_agent)) { 
            $browser_name = 'Mozilla Firefox'; 
            $user_a = "Firefox"; 
        }elseif(preg_match('/OPR/i',$u_agent)){ 
            $browser_name = 'Opera'; 
            $user_a = "Opera"; 
        }elseif(preg_match('/Chrome/i',$u_agent)){ 
            $browser_name = 'Google Chrome'; 
            $user_a = "Chrome"; 
        }elseif(preg_match('/Safari/i',$u_agent)){ 
            $browser_name = 'Apple Safari'; 
            $user_a = "Safari"; 
        }elseif(preg_match('/Netscape/i',$u_agent)) { 
            $browser_name = 'Netscape'; 
            $user_a = "Netscape"; 
        } 
        $known = ['Version', $user_a, 'other'];
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        preg_match($pattern, $u_agent, $matches);
        $i = count($matches['browser']);
        if ($i != 1) {
            if (strripos($u_agent,"Version") < strripos($u_agent,$user_a)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }
        if ($version==null || $version==""){
            $version="not accessable";
         }
        return [
            'userAgent' => $u_agent,
            'name'      => $browser_name,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
       ];
    } 

}
