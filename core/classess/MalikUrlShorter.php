<?php
class URLShorter
{
	private $url;
	private $slug;
	private $type = 'shorturl';
	private $subtype;
	private $ads;
	private $limit = 6;
		/**
		 * Initialize the objects.
		 *
		 * @return void
		 */		
	public function InitObject(){
		$this->Database = new Database;
		$this->Site = new Site;
		$this->Strings = new Strings;
		$this->Json = new Json;
	}
	public function MalikSetUrl($url){
		$this->url = $url;
	}
	public function MalikGetUrl(){
		return $this->url;
	}	
	public function MalikSetAds($ads){
		$this->ads = $ads;
	}
	public function MalikGetAds(){
		return $this->ads;
	}	
	public function MalikGetOId(){
		return null;
	}
	public function MalikSetSubType($subtype){
		if(isset($subtype)){
			$this->subtype = $subtype;
		}else{
			$this->subtype = 'public';
		}
	}
	public function MalikGetSubType(){
		return $this->slug;
	}		
	public function MalikGetImageFURL(){
		//getting url
		$siteURL = $this->url;
		//call Google PageSpeed Insights API
		$googlepsdata = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url={$siteURL}&screenshot=true");
			//decode json data
		$googlepsdata = $this->Json->MalikDecode($googlepsdata,true);
		//screenshot data
		$snap = $googlepsdata['screenshot']['data'];
		$snap = str_replace(array('_','-'),array('/','+'),$snap); 	
		return $snap;
	}
	public function MalikGetTitleFURL(){
		self::InitObject();
		//getting url
		$siteURL = $this->url;
		//call Google PageSpeed Insights API
		$googlepsdata = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url={$siteURL}");
			//decode json data
		$googlepsdata = $this->Json->MalikDecode($googlepsdata,true);
		//screenshot data
		$title = $googlepsdata['title'];
		return $title;
	}	
	public function RandomSlug(){
		self::InitObject();
		return $this->Site->MalikGenerateSalts($this->limit);
	}

	public function MalikPrintData(){
		self::InitObject();
		return [
			'type' => $this->type,
			'subtype' => $this->subtype,
			'owner_id' => $this->MalikGetOId(),
			'url'=> $this->url,
			'slug'=>$this->RandomSlug(),
			'title' => $this->MalikGetTitleFURL(),
			'image_data' => $this->MalikGetImageFURL(),
			'Ads' => $this->MalikGetAds(),
		];
	}	
	public function MalikSetSlug($slug){
		$this->slug = $slug;
	}
	public function MalikGetSlug(){
		return $this->slug;
	}	
	public function MalikGetFDb(){
		self::InitObject();
		return $this->Database->MalikSelectRecord(['from'=>'main','wheres'=>" short_url =".$this->Database->MalikQuote($this->MalikGetSlug())]);
	}
	public function MalikCount(){
		self::InitObject();
		return count($this->Database->MalikSelectRecord(['from'=>'main','wheres'=>" short_url =".$this->Database->MalikQuote($this->MalikGetSlug())]));
	}	
	public function MalikSTObjects($params){
		if(is_array($params)){
		$data = $this->Json->MalikEncode($params['data'],true);
		$this->Database->MalikInsert('objects',[
			'main_id' => $params['main_id'],
			'type' => $this->type,
			'subtype' => 'visitors',
			'data' => $data,
		]);
		return $data;
		}else{
			return false;
		}
	}
	public function MalikSendTDb(){
		self::InitObject();
		$this->Database->MalikInsert("main",
			[
				'type'=>$this->type,
				'subtype'=> $this->subtype,
				'owner_id' => $this->MalikGetOId(),
				'long_url'=> $this->url,
				'short_url'=>$this->RandomSlug(),
				'img_data'=>$this->MalikGetImageFURL(),
				'title_data'=>$this->MalikGetTitleFURL(),
				'Ads' => $this->MalikGetAds(),
			]);
	}
}
