<?php
/**
 * Malik Corporation private limited
 *
 * @package   (my-mos.com)
 * @author    Malik Umer Farooq<admin@my-mos.com>
 * @copyright 2016-2017 Malik Corporation private limited
 * @license  Malik Corporation private limited https://my-mos.com/public/terms
 * @link     https://my-mos.com/public/
 */
class Database
{
		/**
		 * Initialize the objects.
		 *
		 * @return void
		 */	
	public function InitObjects(){
		$this->Zip = new Zip;
		$this->Strings = new Strings;
	}
		/**
		 * Open database connection
		 * @param $status true : false  true means open false colse 
		 * @retu2rn boolean
		 */	 
	public function MalikDbConnection($status){
	    if($status === true){
	        $settings = malik_database_settings();
	        return $db = new PDO($settings['type'] . ':host=' . $settings['host'] . ';dbname=' . $settings['dbname'], $settings['username'], $settings['password']);
	    }
	    if($status === false){
	        return $db = null;
	    }
	 }
		/**
		 * Prepare a query to insert into db
		 * @table Name of tabke 
		 * @param array array(); e.g:
		 *           'name' => 'new name' or $comeformvariable
		 *           'username' => 'new username' or $comeformvariable
		 *
		 * @return integar or boolean
		 */	 	 
	 public function MalikInsert($table,$param){
	 	$db = $this->MalikDbConnection(true);
		$columns = implode(',',array_keys($param));
		$values = ':'.implode(', :',array_keys($param));
		$sql = "INSERT INTO {$table} ({$columns}) values ({$values})";
		if($stmt = $db->prepare($sql)){
			foreach($param as $key => $data){
				$stmt->bindValue(':'.$key,$data);
			}
			$stmt->execute();
			$last =  $db->lastInsertId();
			$db = $this->MalikDbConnection(false);
			return $last;
		}
		return false;
	}
		/**
		 * Prepare a query to select data from database
		 * @table Name of tabke 
		 * @id id for update record
		 * @param array array(); e.g:
		 *           'name' => 'new name' or $comeformvariable
		 *           'username' => 'new username' or $comeformvariable
		 *
		 * @return boolean
		 */	 
	public function MalikUpdate($table,$id,$param){
		if(is_array($param)) {	
				$db = $this->MalikDbConnection(true);
				$columns = '';
				$i = 1;
				foreach($param as $name=>$value){
						$columns .="{$name} = :{$name}";
					if($i < count($param)){
						$columns .=', '; 
					}
					$i++;
				}
				if(!isset($id)){
					$id = 0;
				}
				$sql = "Update {$table} set {$columns} where id= {$id}";
				if($stmt = $db->prepare($sql)){
					foreach($param as $key => $value){
						$stmt->bindValue(':'.$key,$value); 
					}
					$stmt->execute();
				}
				$db = $this->MalikDbConnection(false);
				return true;
			}	return false;
		}

	public function MalikQuote($string){
		return $this->MalikDbConnection(true)->quote($string);
	}		
		/**
		 * Prepare a query to select data from database
		 *
		 * @param array array();
		 *           'from' Names of table
		 *           'params' Names of columns which you want to select
		 *           'wheres' Specify a selection criteria to get required records
		 *            'debug' If on var_dump sql query
		 * @return boolean
		 */	 
	public function MalikSelectRecord($params) {
	    self::InitObjects();
		if(is_array($params)) {
				$db = $this->MalikDbConnection(true);		
					if(!isset($params['params'])) {
							$columns = '*';
					} else {
					    $columns = implode(', ',array_values($params['params']));
					}					
					$wheres = '';
					if(!empty($params['wheres'])) {
					    $wheres = "WHERE " . implode(' and ', array_values($params['wheres']));
					}
					$query = "SELECT {$columns} FROM {$params['from']} {$wheres} ;";
					if(isset($params['debug']) and $this->Strings->MalikStringConversion(['type'=>'lowercase','text'=>$params['debug']]) === 'on' ){
					    var_dump($query);
					}
					$prepare = $db->prepare($query);
					if($prepare->execute()) {
							$data =  malik_array_object($prepare->fetchAll(PDO::FETCH_ASSOC));
							$db = $this->MalikDbConnection(false);
							return $data;
					}
			}
			return false;
	}
	/**
	 * Prepare a query to delete data from database
	 *
	 * @param $params array array();
	 *           'from' Names of table
	 *           'wheres' Specify a selection criteria to get required records
	 *
	 * @return boolean
	 */	
	public function MalikDeleteRecord($params) {
			if(is_array($params)) {
					$db = $this->MalikDbConnection(true);			
					$where =  $params['wheres'];
					if(!empty($params['wheres'])) {
							$wheres = "WHERE({$where})";
					}
					//don't let any query to empty entire table
					if(empty($params['wheres'])) {
							return false;
					}
					$query = "DELETE FROM `{$params['from']}` {$wheres};";
					$prepare = $db->prepare($query);
					if($prepare->execute()) {
							$db = $this->MalikDbConnection(false);
							return true;
					}
			}
			return false;
	}
		/**
		 * Prepare a query to count data from database
		 *
		 * @param $params array();
		 *           'from' Names of table
		 *           'columns' Names of columnswant to select
		 *           'wheres' Specify a selection 		 *       
		 * @return boolean
		 */	 	
	public function MalikCountRecord($params){
		if(is_array($params)){
			$table = $params['table'];
			$db = $this->MalikDbConnection(true);
			if(isset($params['columns'])){
				$columns = implode(',',array_values($params['columns']));
			}else{
				$columns = '*';
			}
			if(!empty($params['wheres'])){
		    	$where = "WHERE " . implode(' and ', array_values($params['wheres']));
				$sql = "SELECT {$columns} FROM {$table} {$where}";
				$prepare = $db->prepare($sql);
				$prepare->execute();
				$count = $prepare->rowCount();
				$db = $this->MalikDbConnection(false);
				return $count;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
} 
