<?php
/**
 * Malik Corporation private limited
 *
 * @package   (my-mos.com)
 * @author    Malik Umer Farooq<admin@my-mos.com>
 * @copyright 2016-2017 Malik Corporation private limited
 * @license  Malik Corporation private limited https://my-mos.com/public/terms
 * @link     https://my-mos.com/public/
 */
class Json{
		/**
		 * Encode data in json format
		 * @data provide data or file tobe encoded
		 * @pretty if true so enable JSON_PRETTY_PRINT
		 * @return boolean
		*/	 
	public function MalikEncode($data, $pretty=false){
			if($pretty === true){
				return json_encode($data,JSON_PRETTY_PRINT);
			}if($pretty === false){
				return json_encode($data);
			}	
	}
		/**
		 * Decode data in json format
		 * @json provide data or file tobe decode
		 * @assoc if convert to assos array
		 * @return boolean
		*/	 
	public function MalikDecode($json, $assoc = false){
			return json_decode($json, $assoc);
	}
}
