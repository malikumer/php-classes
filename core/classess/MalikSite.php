<?php
/**
 * Malik Corporation private limited
 *
 * @package   (my-mos.com)
 * @author    Malik Umer Farooq<admin@my-mos.com>
 * @copyright 2016-2017 Malik Corporation private limited
 * @license  Malik Corporation private limited https://my-mos.com/public/terms
 * @link     https://my-mos.com/public/
 */
class Site
{
		/**
		 * Initialize the objects.
		 *
		 * @return void
		 */		
	public function InitObjects(){
		$this->Database = new Database;
		$this->Strings = new Strings;
	}
		/**
		 * Return site URL
		 * 
		 * @return string
		 */	
	public function MalikSiteUrl(){
		if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off') {
			$protocol = 'http://';
		} else {
			$protocol = 'https://';
		}
		    $base_url = $protocol . $_SERVER['SERVER_NAME'] . dirname($_SERVER['PHP_SELF']);
			return $base_url;
	}
		/**
		 * generate salts for files
		 * 
		 * @param string $length length of salts
		 * @return string
		 */			
	public function MalikGenerateSalts($length){
		$somestrings = '0123456789abcdefghijklmnFGSGSGFGSVHVEHDSHVHVSVHDVGFDopqfgsfsfsfsfrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$stringlength = strlen($somestrings);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $somestrings[rand(0, $stringlength - 1)];
		}
		return $randomString;
	}
		/**
		 * Get language name form db
		 * 
		 * @return string
		 */			
	public function MalikGetLangFromDb(){
        self::InitObjects();
        $language = $this->Database->MalikSelectRecord(['from'=>'malik_settings','params'=>['language']]);
        return $language;
	}	
		/**
		 * include lang string file
		 * 
		 * @return string
		 */			
	public function MalikLanguage(){
	    $language = $this->MalikGetLangFromDb()[0]['language'];
			if(file_exists(getcwd()."/en.php")){
			 require_once getcwd()."/en.php";
			if(is_array($GLOBALS['lang'])){
				return $GLOBALS['lang'];
			}else{
				return [];
			
			}}else{
				return false;
			}
		}	
		/**
		 * for getting language key and return its value
		 * @param $key language key
		 * @return string
		 */
	public function MalikLangPrint($key){
		self::InitObjects();
		if(!empty($key)){
			if(array_key_exists($this->Strings->MalikStringConversion(['type'=>'lowercase','text'=>$key]),$this->MalikLanguage())){
				return $this->MalikLanguage()[$this->Strings->MalikStringConversion(['type'=>'lowercase','text'=>$key])];
			}else{
				return $this->Strings->MalikStringConversion(['type'=>'lowercase','text'=>$key]);
			}
		}else{
			return false;
		}
	}
		/**
		 * Only for debug purpose
		 * 
		 * @param =>$params (array)
		 * 'allkeys'=>'on' ==> return all keys in array
		 * 'search' => 'value' ==> return boolean true on fime false not find Note: it only keys string in language file
		 * @return string
		 */		
	public function MalikLangDebug($params){
		self::InitObjects();
		if(is_array($params)){
			if(isset($params['allkeys']) and $this->Strings->MalikStringConversion(['type'=>'lowercase','text'=> $params['allkeys']]) === 'on'){
				return array_keys($this->MalikLanguage());
			}
			if(isset($params['search'])){
			   if( array_key_exists($params['search'], $this->MalikLanguage())){
			        return true;        
			    }else{
			        return false;
			    }
			}
		}else{
			return false;
		}
	}
}