<?php
/**
 * Malik Corporation private limited
 *
 * @package   (my-mos.com)
 * @author    Malik Umer Farooq<admin@my-mos.com>
 * @copyright 2016-2017 Malik Corporation private limited
 * @license  Malik Corporation private limited https://my-mos.com/public/terms
 * @link     https://my-mos.com/public/
 */
	$dir =  dirname(__FILE__).'/';	
	$disk_scan = array_diff(scandir($dir,1),array('..','.'));
	foreach($disk_scan as $scans){
		require_once $dir.$scans ;
	}
