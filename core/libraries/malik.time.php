<?php
/**
 * Calculate the age of users
 *
 * @param (date) date of birth of user
 *
 * @return integer
 */
function malik_calculate_age($dob){
	$dob = new DateTime($dob);
	$today = new DateTime("today");
	return $dob->diff($today)->y;
}
/**
 * Calculate the time in timestamp ago format 
 *
 * @param (timestamp) timestamp of value
 *
 * @return integar
 */
function malik_friendly_time($timestamp){
	$time = new Time;
	return $time->MalikTimeAgo($timestamp);

}
/**
 * Calculate the execution time of script
 *
 * @param 
 * $start => start time of script $start = microtime(true);
 * $end => end time of script $end = microtime(true);
 * $round Optional for round the return value e.g 2,3,5 etc
 *	
 * @return integar
 */
function malik_executuion_script($start,$end,$round = null){
	$time = new Time;
	if(!isset($round)){
		$round = '';
	}
	return $time->MalikExecutionTime(['start'=>$start,'end'=>$end,'round'=>$round]);

}