<?php	 
/**
 * Set database settings
 *
 * @return array
 */
function malik_database_settings(){
	global $Malik;
	$settings = [
	    'type' => 'mysql',
		'host' => $Malik['host'],
		'username' => $Malik['username'],
		'port' => $Malik['port'],
		'dbname' => $Malik['dbname'],
		'password' => $Malik['password'],
	];
	return $settings;
}
function malik_Data_Dir_settings(){
	global $Malik;
	$settings = [
		'MalikDataDir' => $Malik['MalikDataDir'],
	];
	return $settings;
}
/**
 * Convert arrays to Object
 *
 * @param array $array Arrays
 *
 * @return object
 */
function malik_array_object($array){
	$object = new StdClass;
	foreach ($array as $key => $value) {
			if(is_array($value)){
				$object->{$key} = malik_array_object($value);
			}else{
				$object->{$key} = $value;
			}
	}
	return $object;
}
/**
 * Convert Objects to arrays
 *
 * @param array $array Arrays
 *
 * @return array
 */
function malik_object_array($object){
	$array = [];
	foreach ($object as $key => $value) {
			if(is_object($value)){
				$array["{$key}"] = malik_object_array($value);
			}else{
				$array["{$key}"] = $value;
			}
	}
	return $array;
}
/**
 * Convert XML to arrays
 *
 * @param xml object $xml xml
 *
 * @return array
 */
function malik_xml_array($xml){
	$josn = new Json;
	$dom = simplexml_load_file($xml);
	$json_encode = $josn->MalikEncode($dom);
	$json_decode = $josn->MalikDecode($json_encode,true);
	return $json_decode;
}
		/**
		 * Class hanadler
		 * $params =>
		 * 'type' class and method
		 * 'class_name' => name of calass
		 * 'class_methods' => return array all method of class.
		 * 'class_name_dynamic' => return class name
		 * @return boolean
		*/	 	
function malik_class_hanadler($params){
		if(is_array($params)){
			if(isset($params['type'])){
				if($params['type'] === 'class'){
					$class_name = $params['class_name'];
					if(class_exists($class_name)){
						return true;
					}else{
						return false;
					}
				}
			if(isset($params['class_methods'])){
					return get_class_methods($params['class_methods']);
			}
			if(isset($params['class_name_dynamic'])){
					return get_class($params['class_name_dynamic']);
			}
		} else{
			return false;
		}
	}
}
/**
 * Site url
 *
 * @return string
 */
function malik_site_url(){
		if(malik_class_hanadler(['type'=>'class','class_name'=>'Site'])){
			$url = new Site;
			return $url->MalikSiteUrl();
		}else{
			return false;
		}
}
/**
 * print language string
 *
 * @param (string) $key key of language 
 *
 * @return string
 */
function malik_echo($key){
		if(malik_class_hanadler(['type'=>'class','class_name'=>'Site'])){
			$url = new Site;
			return $url->MalikLangPrint($key);
		}else{
			return false;
		}
}
