<?php
/**
 * Get a user by id
 *
 * @param (integar) Id of user
 *
 * @return aray
 */
function malik_user_by_id($id) {
		$user = new User;
		$id = ['id' => $id];		
		return $user->MalikGetUser($id);
}
/**
 * Get a user by username
 *
 * @param (string) Username of user
 *
 * @return aray
 */
function malik_user_by_username($username) {
		$user = new User;
		$username = ['username' => $username];
		return $user->MalikGetUser($username);
}
/**
 * Get a user by email
 *
 * @param (string) Email of user
 *
 * @return aray
 */
function malik_user_by_email($email) {
		$user = new User;
		$email = ['email' => $email];
		return $user->MalikGetUser($email);
}